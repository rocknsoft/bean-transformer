/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform;

import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * An interface to transform a bean of type S into a bean of type T
 * @param <S>
 *            Source Type
 * 
 * @author Waseem Fadel
 * @since May 07, 2013
 */
public interface Transformer<S, T> {
	/**
	 * Transform source into type T. T should have a default none arguments constructor
	 * 
	 * @param source
	 *            the source bean to convert
	 * @return the target bean of type T
	 */
	T transform(S source) throws TransformException;
}
