/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.helper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * A class that provides helper methods for reflection usage
 * 
 * @author Waseem Fadel
 * @since May 9, 2013
 */
public class ReflectionHelper {

	/**
	 * A helper method to set a value to a field
	 * 
	 * @param targetField
	 *            the target field
	 * @param targetObject
	 *            the target object
	 * @param value
	 *            the value to set
	 * @throws Exception
	 */
	public static void setFieldValue(Field targetField, Object targetObject, Object value) throws Exception {
		Class<? extends Object> targetClass = targetObject.getClass();
		Method method = null;
		String methodName = getSetterMethod(targetField.getName());
		while (method == null && targetClass != null) {
			try {
				method = targetClass.getDeclaredMethod(methodName, targetField.getType());
			} catch (Exception e) {
				targetClass = targetClass.getSuperclass();
			}
		}
		if (method == null) {
			throw new NoSuchMethodException(methodName);
		}
		method.invoke(targetObject, value);
	}

	/**
	 * A helper method that builds the name of the getter method of the provided
	 * field
	 * 
	 * @param name
	 *            the name of the field
	 * @return the name of the getter method
	 */
	public static String getGetterMethod(String name) {
		return "get" + getFirstLetterUpper(name);
	}

	/**
	 * A helper method that builds the name of the getter method of the provided
	 * field
	 * 
	 * @param name
	 *            the name of the field
	 * @return the name of the getter method
	 */
	public static String getGetterMethod(String name, Class<?> type) {
		if (type == boolean.class){
			return "is" + getFirstLetterUpper(name);
		}
		return getGetterMethod(name);
	}

	/**
	 * A helper method that builds the name of the setter method of the provided
	 * field
	 * 
	 * @param name
	 *            the name of the field
	 * @return the name of the setter method
	 */
	public static String getSetterMethod(String name) {
		return "set" + getFirstLetterUpper(name);
	}

	private static String getFirstLetterUpper(String value) {
		return value.substring(0, 1).toUpperCase() + value.substring(1);
	}
	
	public static String getFirstLetterLower(String value) {
		return value.substring(0, 1).toLowerCase() + value.substring(1);
	}
	
}
