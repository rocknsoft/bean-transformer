/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.Transformable;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms a collection to a targetField
 * 
 * @author Waseem Fadel
 * @since May 8, 2013
 */
@TransformerMeta(name = "collectionTransformer")
public class CollectionTransformer<S, T> extends AbstractTransformer<S, T> {
	private Logger logger = LoggerFactory.getLogger(CollectionTransformer.class);

	public CollectionTransformer(TransformerContext context) {
		super(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.rocknsoft.util.conversion.Publisher#publish(java.lang.Object)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public T transform(S source) {
		logger.info("publish(targetField, targetObjet, targetType, value)");
		try {
			Object result = null;
			if (source instanceof List) {
				logger.info("Creating target object of type List");
				List list = (List) source;
				result = result == null ? new ArrayList() : result;
				for (Object o : list) {
					((List) result).add(getTransformedObject(o));
				}
			} else if (source instanceof Set) {
				logger.info("Creating target object of type Set");
				Set set = (Set) source;
				result = result == null ? new HashSet() : result;
				for (Object o : set) {
					((Set) result).add(getTransformedObject(o));
				}
			} else if (source instanceof Map) {
				logger.info("Creating target object of type Map");
				Map map = (Map) source;
				result = result == null ? new HashMap() : result;
				for (Object o : map.keySet()) {
					((Map) result).put(o, getTransformedObject(map.get(o)));
				}
			} else if (source instanceof Queue) {
				logger.info("Creating target object of type Queue");
				Queue queue = (Queue) source;
				result = result == null ? new ArrayDeque() : result;
				for (Object o : queue) {
					((Queue) result).add(getTransformedObject(o));
				}
			}
			return (T)result;
		} catch (Exception e) {
			String msg = "Could not transform bean!";
			logger.error(msg, e);
			throw new TransformException(msg, e);
		}
	}
	
	private Object getTransformedObject(Object o){
		if (o.getClass().getAnnotation(Transformable.class) != null) {
			return context.transform(o, null, TransformableTransformer.class);
		}
		return context.transform(o, null, PrimitiveTransformer.class);
	}

}
