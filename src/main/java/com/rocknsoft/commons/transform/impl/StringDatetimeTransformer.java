/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms String into Date
 * 
 * @author Waseem Fadel
 * @since Apr 9, 2014
 */
@TransformerMeta(name = "stringDatetimeTransformer")
public class StringDatetimeTransformer extends AbstractTransformer<String, Date> {

	private String pattern;

	public StringDatetimeTransformer(TransformerContext context) {
		super(context);
	}

	@Override
	public Date transform(String source) throws TransformException {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		try {
			if (source != null && !source.isEmpty()) {
				return formatter.parse(source);
			} else {
				return null;
			}
		} catch (ParseException e) {
			throw new TransformException("Could not transform!", e);
		}
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
}
