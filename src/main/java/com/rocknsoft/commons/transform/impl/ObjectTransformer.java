/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;
import com.rocknsoft.commons.transform.helper.ReflectionHelper;

/**
 * A transformer that transforms S into T
 * 
 * @author Waseem Fadel
 * @since Jan 20, 2014
 */
@TransformerMeta(name = "objectTransfomer")
public class ObjectTransformer<S, T> extends AbstractTransformer<S, T> {

	public ObjectTransformer(TransformerContext context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T transform(S source) throws TransformException {
		try {
			T result = (T) source.getClass().newInstance();
			for (Field sourceField : source.getClass().getDeclaredFields()) {
				if (!(Modifier.isStatic(sourceField.getModifiers()) && Modifier.isFinal(sourceField.getModifiers()))) {
					ReflectionHelper.setFieldValue(sourceField, result,
							source.getClass().getDeclaredMethod(ReflectionHelper.getGetterMethod(sourceField.getName(), sourceField.getType())).invoke(source));
				}
			}
			return result;
		} catch (Exception e) {
			throw new TransformException("Could not transform", e);
		}
	}

}
