/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms primitive types (String, Double, Integer, Float, Boolean, Byte, Short, Long)
 *
 * @author Waseem Fadel
 * @since Jan 09, 2014
 * @param <S>
 * @param <T>
 */
@TransformerMeta(name = "primitiveTransformer")
public class PrimitiveTransformer<S, T> extends AbstractTransformer<S, T> {

	public PrimitiveTransformer(TransformerContext context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T transform(S source) throws TransformException {
		try {
			T result = null;
			result = (T)source.getClass().getConstructor(String.class).newInstance(String.valueOf(source));
			return result;
		} catch (Exception e) {
			throw new TransformException("Could not transform", e);
		}
	}

}
