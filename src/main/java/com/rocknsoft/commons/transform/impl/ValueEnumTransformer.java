/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import java.io.Serializable;

import com.rocknsoft.commons.RocknsoftEnum;
import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms values into enum types
 * 
 * @author Waseem Fadel
 * @since Aug 13, 2014
 * @param <S>
 * @param <T>
 */
@TransformerMeta(name = "valueEnumTransformer")
public class ValueEnumTransformer<S extends Serializable, T extends RocknsoftEnum<S>> extends AbstractTransformer<S, T> {
	private Class<T> enumClass;

	public ValueEnumTransformer(TransformerContext context) {
		super(context);
	}

	public void setEnumClass(Class<T> enumClass) {
		this.enumClass = enumClass;
	}

	@Override
	public T transform(S source) throws TransformException {
		try {
			for (T t: enumClass.getEnumConstants()){
				if (t.getValue().equals(source)) {
					return t;
				}
			}
			return null;
		} catch (Exception e) {
			throw new TransformException("Could not transform", e);
		}
	}
}
