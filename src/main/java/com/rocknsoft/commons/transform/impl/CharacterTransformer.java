/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms Character object into Character object
 * @author Waseem Fadel
 * @since Jan 20, 2014
 */
@TransformerMeta (name = "characterTransformer")
public class CharacterTransformer extends AbstractTransformer<Character, Character> {

	public CharacterTransformer(TransformerContext context) {
		super(context);
	}

	@Override
	public Character transform(Character source) throws TransformException {
		try {
			return (Character) source.getClass().getConstructor(char.class).newInstance(source);
		} catch (Exception e) {
			throw new TransformException("Could not transform", e);
		}
	}

}
