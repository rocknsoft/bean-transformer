/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms byte array into String
 * 
 * @author Waseem Fadel
 * @since Jan 20, 2014
 */
@TransformerMeta(name = "byteArrayStringTransformer")
public class ByteArrayToStringTransformer extends AbstractTransformer<byte[], String> {
	private Logger logger = LoggerFactory.getLogger(ByteArrayToStringTransformer.class);

	public ByteArrayToStringTransformer(TransformerContext context) {
		super(context);
	}

	@Override
	public String transform(byte[] source) throws TransformException {
		logger.info("transform(source)");
		logger.debug("Transforming object of type: " + source.getClass().getSimpleName());
		String result = "";
		result = Base64.encodeBase64URLSafeString(source);
		return result;
	}

}
