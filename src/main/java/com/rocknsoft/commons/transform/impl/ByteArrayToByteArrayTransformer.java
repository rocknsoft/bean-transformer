/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;

/**
 * A transformer that transforms byte array into byte array
 * 
 * @author Waseem Fadel
 * @since Jan 20, 2014
 */
@TransformerMeta(name = "byteArrayByteArrayTransformer")
public class ByteArrayToByteArrayTransformer extends AbstractTransformer<byte[], byte[]>{
	private Logger logger = LoggerFactory.getLogger(ByteArrayToByteArrayTransformer.class);

	public ByteArrayToByteArrayTransformer(TransformerContext context) {
		super(context);
	}

	@Override
	public byte[] transform(byte[] source) throws TransformException {
		logger.info("transform(source)");
		logger.debug("Transforming object of type: " + source.getClass().getSimpleName());
		byte [] result = new byte[source.length];
		try {
			for (int i =0; i < source.length; i++) {
				result[i] = source[i];
			}
		} catch (Exception e) {
			String msg = "Could not transform bean!";
			logger.error(msg, e);
			throw new TransformException(msg, e);
		}
		return result;
	}

}
