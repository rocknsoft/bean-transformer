/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rocknsoft.commons.transform.AbstractTransformer;
import com.rocknsoft.commons.transform.annotation.Transform;
import com.rocknsoft.commons.transform.annotation.Transformable;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.context.TransformerContext;
import com.rocknsoft.commons.transform.exception.TransformException;
import com.rocknsoft.commons.transform.helper.ReflectionHelper;

/**
 * A transformer that transforms a bean of type S annotated with
 * {@link Transformable} to a bean of type T
 * 
 * @author Waseem Fadel
 * @since May 07, 2013
 */
@TransformerMeta(name = "transformableTransformer")
public class TransformableTransformer<S, T> extends AbstractTransformer<S, T> {
	private Logger logger = LoggerFactory.getLogger(TransformableTransformer.class);

	public TransformableTransformer(TransformerContext context) {
		super(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.rocknsoft.commons.transform.Transformer#transform(S)
	 */
	@Override
	public T transform(S source) throws TransformException {
		T targetObject = null;
		String targetFieldName = "";
		if (source != null) {
			try {
				Class<? extends Object> sourceClass = source.getClass();
				Transformable transform = sourceClass.getAnnotation(Transformable.class);
				targetObject = createTargetObject(transform);
				Class<? extends Object> targetClass = targetObject.getClass();
				while (sourceClass != null) {
					for (Field sourceField : sourceClass.getDeclaredFields()) {
						Transform transformTo = sourceField.getAnnotation(Transform.class);
						if (transformTo != null) {
							targetFieldName = transformTo.name().equals("") ? sourceField.getName() : transformTo.name();
							Field targetField = null;
							while (targetField == null && targetClass != null) {
								try {
									targetField = targetClass.getDeclaredField(targetFieldName);
								} catch (Exception e) {
									targetClass = targetClass.getSuperclass();
								}
							}
							if (targetField == null) {
								throw new NoSuchFieldException("Field " + targetFieldName + " was not found in target bean!");
							}
							targetClass = targetObject.getClass();
							Class<?> using = transformTo.using();
							Object sourceFieldObject = sourceClass.getMethod(ReflectionHelper.getGetterMethod(sourceField.getName(), sourceField.getType())).invoke(source);
							if (sourceFieldObject != null) {
								if (!transformTo.sourceName().equals("")) {
									sourceFieldObject = sourceFieldObject.getClass().getMethod(ReflectionHelper.getGetterMethod(transformTo.sourceName(), sourceField.getType()))
											.invoke(sourceFieldObject);
								}

								// By Tammam -- to look for better solution in
								// future
								if ("getOrCreate".equals(context.getPersistenceFetchMethod()) && using != null && PersistenceTransformer.class.getSimpleName().equals(using.getSimpleName())
										&& sourceFieldObject == null) {
									// the entity object is new and pk is null,
									// then it is NOT fetched from DB and has to
									// be populated from field dto
									sourceFieldObject = sourceClass.getMethod(ReflectionHelper.getGetterMethod(sourceField.getName(), sourceField.getType())).invoke(source);
									if (sourceFieldObject != null) {
										Object targetFieldObject = context.transform(sourceFieldObject);
										ReflectionHelper.setFieldValue(targetField, targetObject, targetFieldObject);
										// the relation master->detail will be
										// missing the detail pointing to master
										// in the persistence entity, here we
										// set it
										try {
											// supposing the detail has a field
											// referring to the master Class
											// using the className as fieldName
											// but starting with small letter
											Field tgf = targetFieldObject.getClass().getDeclaredField(ReflectionHelper.getFirstLetterLower(targetObject.getClass().getSimpleName()));
											ReflectionHelper.setFieldValue(tgf, targetFieldObject, targetObject);
										} catch (Exception ex) {
											logger.warn("relation ship child to parent could not be verified: " + ex);
										}

									}
								} else if ("getOrCreate".equals(context.getPersistenceFetchMethod()) && using != null
										&& PersistenceCollectionTransformer.class.getSimpleName().equals(using.getSimpleName())) {
									// the entity object is new and pk is null,
									// then it is NOT fetched from DB and has to
									// be populated from field dto
									if (sourceFieldObject != null) {
										List<Object> targetFieldObject = context.transform(sourceFieldObject, targetField, using);
										ReflectionHelper.setFieldValue(targetField, targetObject, targetFieldObject);
										for (Object targetOneFieldObject : targetFieldObject) {
											// the relation master->detail will
											// be missing the detail pointing to
											// master in the persistence entity,
											// here we set it
											try {
												// supposing the detail has a
												// field referring to the master
												// Class using the className as
												// fieldName but starting with
												// small letter
												Field tgf = targetOneFieldObject.getClass().getDeclaredField(ReflectionHelper.getFirstLetterLower(targetObject.getClass().getSimpleName()));
												ReflectionHelper.setFieldValue(tgf, targetOneFieldObject, targetObject);
											} catch (Exception ex) {
												logger.warn("relation ship child to parent could not be verified: " + ex);
											}
										}
									}
								} else {
									Object targetFieldObject = null;
									// ------------------------------------------------------
									if (sourceFieldObject == null && using != null && PersistenceTransformer.class.getSimpleName().equals(using.getSimpleName())) {
										// if Persistence, then the entity
										// fetched using getBean should be
										// merged with dto
										// the relation master->detail will be
										// missing the detail pointing to master
										// in the persistence entity, here we
										// set it
										System.out.println(sourceFieldObject);
										try {
											// supposing the detail has a field
											// referring to the master Class
											// using the className as fieldName
											// but starting with small letter
											sourceFieldObject = sourceClass.getMethod(ReflectionHelper.getGetterMethod(sourceField.getName(), sourceField.getType())).invoke(source);
											targetFieldObject = context.transform(sourceFieldObject);
											ReflectionHelper.setFieldValue(targetField, targetObject, targetFieldObject);
											Field tgf = targetFieldObject.getClass().getDeclaredField(ReflectionHelper.getFirstLetterLower(targetObject.getClass().getSimpleName()));
											ReflectionHelper.setFieldValue(tgf, targetFieldObject, targetObject);
										} catch (Exception ex) {
											logger.warn("relation ship child to parent could not be verified: " + ex);
										}
									} else {
										// this is the original code outside the
										// if else block
										targetFieldObject = context.transform(sourceFieldObject, targetField, using);
									}
									// ------------------------------------------------------

									ReflectionHelper.setFieldValue(targetField, targetObject, targetFieldObject);

								}

							}
						}
					}
					sourceClass = sourceClass.getSuperclass();
				}
			} catch (Exception e) {
				String msg = "Could not transform bean of type " + source.getClass() + "!\n An error occured when transfroming property " + targetFieldName;
				logger.error(msg, e);
				throw new TransformException(msg, e);
			}
		}
		return targetObject;
	}

	@SuppressWarnings("unchecked")
	private T createTargetObject(Transformable transform) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		T targetObject;
		if (transform.factoryMethod() == null || transform.factoryMethod().isEmpty()) {
			if (transform.target() != Object.class) {
				targetObject = (T) transform.target().newInstance();
			} else if (!transform.targetName().equals("")) {
				try {
					targetObject = (T) Class.forName(transform.targetName()).newInstance();
				} catch (ClassNotFoundException e) {
					String msg = "Target class of type " + transform.targetName() + " not found exception!";
					logger.error(msg, e);
					throw new TransformException(msg, e);
				}
			} else {
				String msg = "No target class is provided!";
				logger.error(msg);
				throw new TransformException(msg);
			}
		} else {
			Object factoryObject = transform.factoryClass().newInstance();
			@SuppressWarnings("rawtypes")
			List<Object> params = new ArrayList();
			int[] counters = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			for (Class<?> clazz : transform.factoryMethodParameterTypes()) {
				if (clazz == boolean.class || clazz == Boolean.class) {
					params.add(transform.factoryMethodBooleanParams()[counters[0]++]);
				} else if (clazz == char.class || clazz == Character.class) {
					params.add(transform.factoryMethodCharParams()[counters[1]++]);
				} else if (clazz == byte.class || clazz == Byte.class) {
					params.add(transform.factoryMethodByteParams()[counters[2]++]);
				} else if (clazz == double.class || clazz == Double.class) {
					params.add(transform.factoryMethodDoubleParams()[counters[3]++]);
				} else if (clazz == float.class || clazz == Float.class) {
					params.add(transform.factoryMethodFloatParams()[counters[4]++]);
				} else if (clazz == int.class || clazz == Integer.class) {
					params.add(transform.factoryMethodIntParams()[counters[5]++]);
				} else if (clazz == long.class || clazz == Long.class) {
					params.add(transform.factoryMethodLongParams()[counters[6]++]);
				} else if (clazz == short.class || clazz == Short.class) {
					params.add(transform.factoryMethodShortParams()[counters[7]++]);
				} else if (clazz == String.class) {
					params.add(transform.factoryMethodStringParams()[counters[8]++]);
				} else {
					params.add(transform.factoryMethodClassParams()[counters[9]++]);
				}
			}
			targetObject = (T) transform.factoryClass().getMethod(transform.factoryMethod(), transform.factoryMethodParameterTypes()).invoke(factoryObject, params.toArray());
		}
		return targetObject;
	}

}
