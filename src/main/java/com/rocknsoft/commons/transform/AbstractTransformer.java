/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform;

import com.rocknsoft.commons.transform.context.TransformerContext;

/**
 * An abstract class that holds the context reference for all transformers
 * @param <S>
 *            Source Type
 * 
 * @author Waseem Fadel
 * @since January 09, 2014
 */
public abstract class AbstractTransformer<S, T> implements Transformer<S, T> {
	protected TransformerContext context;
	
	public AbstractTransformer(TransformerContext context) {
		this.context = context;
	}
}
