/**
 * 
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.rocknsoft.commons.transform.Transformer;
import com.rocknsoft.commons.transform.impl.PrimitiveTransformer;

/**
 * An annotation that defines the rules for the {@link Transformer} to transform a source field to a target field
 * 
 * @author Waseem Fadel
 * @since May 07, 2013
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
public @interface Transform {
	/**
	 * @return the source field name in the source property to be transformed to target field
	 */
	String sourceName() default "";
	/**
	 * @return Target field name - mandatory
	 */
	String name() default "";
	
	/**
	 * @return the transformer type to use
	 */
	Class<?> using() default PrimitiveTransformer.class;
}
