/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.rocknsoft.commons.transform.Transformer;

/**
 * An annotation that defines the type of the target bean for {@link Transformer} to create
 * @author Waseem Fadel
 * @since May 07, 2013
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface Transformable {
	/**
	 * @return the type of the target bean
	 */
	Class<?> target() default Object.class;
	/**
	 * @return the type of the target bean
	 */
	String targetName() default "";
	/**
	 * @return the factory class 
	 */
	Class<?> factoryClass() default Object.class;
	/**
	 * @return the factory method in the factory class that creates the target object
	 */
	String factoryMethod() default "";
	
	/**
	 * @return the parameters types of the factor method
	 */
	Class<?>[] factoryMethodParameterTypes() default {};

	/**
	 * @return the list of byte parameters that should be passed to the factory method
	 */
	byte[] factoryMethodByteParams() default {};

	/**
	 * @return the list of int parameters that should be passed to the factory method
	 */
	int[] factoryMethodIntParams() default {};

	/**
	 * @return the list of short parameters that should be passed to the factory method
	 */
	short[] factoryMethodShortParams() default {};

	/**
	 * @return the list of long parameters that should be passed to the factory method
	 */
	long[] factoryMethodLongParams() default {};

	/**
	 * @return the list of double parameters that should be passed to the factory method
	 */
	double[] factoryMethodDoubleParams() default {};

	/**
	 * @return the list of float parameters that should be passed to the factory method
	 */
	float[] factoryMethodFloatParams() default {};

	/**
	 * @return the list of boolean parameters that should be passed to the factory method
	 */
	boolean[] factoryMethodBooleanParams() default {};

	/**
	 * @return the list of char parameters that should be passed to the factory method
	 */
	char[] factoryMethodCharParams() default {};

	/**
	 * @return the list of String parameters that should be passed to the factory method
	 */
	String[] factoryMethodStringParams() default {};

	/**
	 * @return the list of class parameters that should be passed to the factory method
	 */
	Class<?>[] factoryMethodClassParams() default {};
}
