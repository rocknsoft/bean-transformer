/*
 * Copyright (C) 2013 the rocknsoft bean transformer project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rocknsoft.commons.transform.context;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rocknsoft.commons.transform.PersistenceDAO;
import com.rocknsoft.commons.transform.Transformer;
import com.rocknsoft.commons.transform.annotation.TransformerMeta;
import com.rocknsoft.commons.transform.exception.TransformException;
import com.rocknsoft.commons.transform.impl.DateStringTransformer;
import com.rocknsoft.commons.transform.impl.DatetimeStringTransformer;
import com.rocknsoft.commons.transform.impl.PersistenceTransformer;
import com.rocknsoft.commons.transform.impl.StringDateTransformer;
import com.rocknsoft.commons.transform.impl.StringDatetimeTransformer;
import com.rocknsoft.commons.transform.impl.TransformableTransformer;
import com.rocknsoft.commons.transform.impl.ValueEnumTransformer;

/**
 * A central class that defines the context in which transformers run. This is
 * the entry point to the framework, and it's the default context through which
 * all transformation should be done
 * 
 * @author Waseem Fadel
 * @since Jan 09, 2014
 */
public class TransformerContext {
	private Map<String, Class<?>> transformers = null;
	private String[] packagesToScan;
	private PersistenceDAO persistenceDAO;
	private String dateFormat;
	private String datetimeFormat;
	private String persistenceFetchMethod = "getOnly"; // getOnly|getOrCreate

	private Logger logger = LoggerFactory.getLogger(TransformerContext.class);

	public TransformerContext() {
	}

	@PostConstruct
	public void init() {
		scanPackages();
	}

	private void scanPackages() {
		transformers = new HashMap<String, Class<?>>();
		scanPackages("com.rocknsoft.commons.transform.impl");
		scanPackages(packagesToScan);
	}

	private void scanPackages(String... packagesToScan) {
		try {
			if (packagesToScan != null && packagesToScan.length > 0) {

				Reflections reflections = null;
				Set<Class<?>> transformerClasses = null;
				/*
				 * BY TAMMAM FADEL the following line does not work with IBM
				 * WebSphere as bean-transformer jar is an external library no
				 * classes are returned from reflection
				 */
				reflections = new Reflections((Object[]) packagesToScan);
				transformerClasses = reflections.getTypesAnnotatedWith(TransformerMeta.class);

				// WORLKAROUND:
				if (transformerClasses.size() == 0) {

					File file = new File(TransformerContext.class.getProtectionDomain().getCodeSource().getLocation().getPath());
					URL jarfile = new URL("jar", "", "file:" + file.getAbsolutePath() + "!/");
					reflections = new Reflections(jarfile);
					transformerClasses = reflections.getTypesAnnotatedWith(TransformerMeta.class);
				}
				// END WORKAROUND

				if (transformerClasses.size() == 0) {
					logger.error("transformerClasses size is zero");
					System.out.println("transformerClasses size is zero");
				}
				for (Class<?> clazz : transformerClasses) {
					transformers.put(clazz.getAnnotation(TransformerMeta.class).name(), clazz);
				}
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public <S, T> T transform(S source) throws TransformException {
		return transform(source, null, TransformableTransformer.class);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <S, T> T transform(S source, Field targetField, Class<?> transformerClass) throws TransformException {
		T result = null;
		try {
			Constructor<Transformer<S, T>> constructor = (Constructor<Transformer<S, T>>) transformers.get(transformerClass.getAnnotation(TransformerMeta.class).name()).getConstructor(
					TransformerContext.class);
			Transformer<S, T> transformer = constructor.newInstance(this);
			if (targetField != null) {
				if (transformer instanceof PersistenceTransformer) {
					((PersistenceTransformer) transformer).setClazz(targetField.getType());
					((PersistenceTransformer) transformer).setPersistenceDAO(persistenceDAO);
				} else if (transformer instanceof DateStringTransformer) {
					((DateStringTransformer) transformer).setPattern(dateFormat);
				} else if (transformer instanceof StringDateTransformer) {
					((StringDateTransformer) transformer).setPattern(dateFormat);
				} else if (transformer instanceof DatetimeStringTransformer) {
					((DatetimeStringTransformer) transformer).setPattern(datetimeFormat);
				} else if (transformer instanceof StringDatetimeTransformer) {
					((StringDatetimeTransformer) transformer).setPattern(datetimeFormat);
				} else if (transformer instanceof ValueEnumTransformer) {
					((ValueEnumTransformer) transformer).setEnumClass(targetField.getType());
				}
			}
			result = transformer.transform(source);
		} catch (Exception e) {
			throw new TransformException(e.getMessage(), e);
		}
		return result;
	}

	public String[] getPackagesToScan() {
		return packagesToScan;
	}

	public void setPackagesToScan(String[] packagesToScan) {
		this.packagesToScan = packagesToScan;
	}

	public PersistenceDAO getPersistenceDAO() {
		return persistenceDAO;
	}

	public void setPersistenceDAO(PersistenceDAO persistenceDAO) {
		this.persistenceDAO = persistenceDAO;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getDatetimeFormat() {
		return datetimeFormat;
	}

	public void setDatetimeFormat(String datetimeFormat) {
		this.datetimeFormat = datetimeFormat;
	}

	public String getPersistenceFetchMethod() {
		return persistenceFetchMethod;
	}

	public void setPersistenceFetchMethod(String persistenceFetchMethod) {
		this.persistenceFetchMethod = persistenceFetchMethod;
	}

}
